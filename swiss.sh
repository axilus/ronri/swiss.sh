#!/bin/bash
# this file is part of swiss.sh which is released under the mit license.
# go to http://opensource.org/licenses/mit for full details.

# set script options.
set -o nounset
set -o errexit

# Setup global variables.
if [[ $(uname -a) =~ Darwin ]]; then readlink() { greadlink "$@"; }; fi # Replace MacOS readlink with POSIX version.
SWISS_ROOT=$(readlink -f ${BASH_SOURCE[0]} | xargs dirname)
SWISS_PATHS=($(find $SWISS_ROOT -type f -name "*.sh" \( -not -name "*_test.sh" -not -name "*swiss.sh" \))) # Create array of all modules excluding tests and this file itself.
SWISS_SOURCEE=$(readlink -f ${BASH_SOURCE[1]})

swiss::is_mac() {
  # Test if operating on a MacOS platform.
  # Globals:
  #   None
  # Parameters:
  #   None
  # Returns:
  #   True: If MacOS detected.
  #   False: Otherwise.
  [[ $(uname -a) =~ "Darwin" ]]
}

swiss::is_linux() {
  # Test if operating on a Linux-based platform.
  # Globals:
  #   None
  # Parameters:
  #   None
  # Returns:
  #   True: If Linux detected.
  #   False: Otherwise.
  [[ $(uname -a) =~ "Linux" ]]
}

swiss::is_windows() {
  # Test if operating on a Windows-based platform.
  # Globals:
  #   None
  # Parameters:
  #   None
  # Returns:
  #   True: If Windows detected.
  #   False: Otherwise.
  case $(uname -a) in
  *microsoft*) true && return ;;
  *wsl*) true && return ;;
  esac

  false
}

swiss::colorize() {
  # apply color to a string.
  # globals:
  #   none
  # arguments:
  #   $1:  color code to use.
  #   $2:  string to colorize.
  # returns:
  #   none.
  # the color codes are as follows:
  #   0: black.
  #   1: red.
  #   2: green.
  #   3: yellow.
  #   4: blue.
  #   5: magenta.
  #   6: cyan.
  #   7: white.
  colors=$(tput colors)
  if test $? && test $colors -ge 8; then  # Check for colour capability.
    echo -e "$(tput setaf ${1})${2}$(tput sgr0)"
  else
    echo -e "${2}"
  fi
}

swiss::main() {
  # source all the swiss modules in the SWISS_ROOT directory.
  # globals:
  #   SWISS_PATHS:
  #   SWISS_SOURCEE:
  # arguments:
  #   none.
  # returns:
  #   none.
  for swiss_path in "${SWISS_PATHS[@]}"; do
    if [[ ${swiss_path} != "${SWISS_SOURCEE}" ]]; then
      source "${swiss_path}"
    fi
  done
}

swiss::main
